﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KoolEntity
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class ManguKoolEntities : DbContext
    {
        public ManguKoolEntities()
            : base("name=ManguKoolEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Aine> Aine { get; set; }
        public virtual DbSet<Hinne> Hinne { get; set; }
        public virtual DbSet<Inimene> Inimene { get; set; }
        public virtual DbSet<Klass> Klass { get; set; }
    }
}
