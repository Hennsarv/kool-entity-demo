﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace KoolEntity
{
    class Program
    {
        static void Main(string[] args)
        {
            //string DataPath = @"..\..\DataFiles\";

            string DataPath = @"C:\Users\sarvi\source\repos\ManguKool\ManguKool\DataFiles\";

            string KlassidFailinimi = DataPath + "Klassid.txt";
            string ÕpilasedFailinimi = DataPath + "Õpilased.txt";
            string ÕpetajadFailinimi = DataPath + "Õpetajad.txt";
            string VanemadFailinimi = DataPath + "Vanemad.txt";
            string AinedFailinimi = DataPath + "Ained.txt";

            using (ManguKoolEntities d = new ManguKoolEntities())
            {
                File.ReadAllLines(KlassidFailinimi)
                    .Select(x => x.Split(','))
                    .Where( x =>  ! (d.Klass.Select(y => y.KlassiKood).Contains(x[0])))
                    .Select(x => new Klass() { KlassiKood = x[0], Nimetus = x.Length > 1 ? x[1].Trim() : "" })
                    .GroupBy(x => x.KlassiKood)
                    .Select (x => x.First())
                    .ToList()
                    .ForEach(x => d.Klass.Add(x));
                d.SaveChanges();

                foreach(var x in d.Klass)
                {
                   Console.WriteLine($"klassis {x.Nimetus} õpib {x.Õpilased.Count} last");
                }

            }


        }
    }
}
